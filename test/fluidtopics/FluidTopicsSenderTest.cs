/*
 * ##########################################################################
 * #                                                                        #
 * #  Copyright (C) 1999-2019 Antidot                                       #
 * #                                                                        #
 * #  Should you receive a copy of this source code, you must check you     #
 * #  have a proper, written authorization of Antidot to hold it. If you    #
 * #  don't have such an authorization, you must DELETE all source code     #
 * #  files in your possession, and inform Antidot of the fact you obtain   #
 * #  these files. Should you not comply to these terms, you can be         #
 * #  prosecuted in the extent permitted by applicable law.                 #
 * #                                                                        #
 * ##########################################################################
 */

using System;
using System.Net.Http;

namespace fluidtopics
{
    class FluidTopicsSenderTest
    {
        static void Main(string[] args)
        {
            //string user   = "<login>";
            //string pwd    = "<password>>";
            //string url    = "<http(s)://my_instance_url/>";
            //string fpath  = "</your/archive/path>";
            string user   = "root@fluidtopics.com";
            string pwd    = "gloubiboulga";
            string url    = "https://your.fluidtopics.net/mwang";
            string fpath  = "/Users/mickaelwang/Documents/copro/AV/zipExample/Dita02.zip";
            string source = "dita";

            FluidTopicsSender client = new FluidTopicsSender(
               url, user, pwd, fpath, source
            );
            Console.WriteLine("Init of enabler OK");
            HttpResponseMessage resp = client.SendToMyInstance();
            Console.WriteLine("Receive status: " + resp.StatusCode);

        }
    }
}