﻿/*
 * ##########################################################################
 * #                                                                        #
 * #  Copyright (C) 1999-2019 Antidot                                       #
 * #                                                                        #
 * #  Should you receive a copy of this source code, you must check you     #
 * #  have a proper, written authorization of Antidot to hold it. If you    #
 * #  don't have such an authorization, you must DELETE all source code     #
 * #  files in your possession, and inform Antidot of the fact you obtain   #
 * #  these files. Should you not comply to these terms, you can be         #
 * #  prosecuted in the extent permitted by applicable law.                 #
 * #                                                                        #
 * ##########################################################################
 *
 */

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace fluidtopics
{
    public class FluidTopicsSender
    {
        string Url   { get; }
        string User  { get; }
        string Pwd   { get; }
        string Fname { get; }
        string Fpath { get; }
        string Auth  { get; }
        string Src { get; set; }
         
        public FluidTopicsSender(string url, string user, string password, string fpath, string source) {
            this.Url   = url;
            this.User  = user;
            this.Pwd   = password;
            this.Fpath = fpath;
            this.Fname = Path.GetFileName(fpath);
            this.Auth  = this.ConvertToBase64(this.User, this.Pwd);
            this.Src   = source;
        }

        public FluidTopicsSender(string url, string auth_64, string fpath, string source) {
            this.Url   = url;
            this.User  = null;
            this.Pwd   = null;
            this.Fpath = fpath;
            this.Fname = Path.GetFileName(fpath);
            this.Auth  = auth_64;
            this.Src   = source;
        }

        string ConvertToBase64(string user, string password){
            return Convert.ToBase64String(
                Encoding.GetEncoding("ISO-8859-1").GetBytes(
                    user + ":" + password
                )
            );
        }

        MultipartFormDataContent BuildMultiPart(string disp_filename=null) {
            MultipartFormDataContent form = new MultipartFormDataContent();

            FileStream fs = File.OpenRead(this.Fpath);
            var streamContent = new StreamContent(fs);

            var fileContent = new ByteArrayContent(
                streamContent.ReadAsByteArrayAsync().Result
            );

            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse(
                "multipart/form-data"
            );
            if (disp_filename == null) {
                disp_filename = "Export from C# plugins for" + this.Fname;
            }
            form.Add(fileContent, disp_filename, this.Fname);
            return form;
        }

        string get_ws() {
            string url = this.Url;
            if (!url.EndsWith("/", StringComparison.Ordinal)) {
                url += "/";
            }
            return url + "api/admin/khub/sources/" + this.Src + "/upload";
        }

        public HttpResponseMessage SendToMyInstance(string disp_filename = null) {
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add(
                "Authorization",
                String.Format("Basic {0}", this.Auth)
            );

            MultipartFormDataContent form = this.BuildMultiPart(disp_filename);

            return httpClient.PostAsync(
                this.get_ws(),
                form
            ).Result;
        }
    }
}